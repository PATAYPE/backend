package com.proempresa.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proempresa.app.entity.Cliente;
import com.proempresa.app.service.ClienteService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/cliente")
public class ClienteController {

	
	@Autowired
	private ClienteService service;
	
	
	@GetMapping
	public ResponseEntity<?> findAll(){
		return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id){
		return ResponseEntity.status(HttpStatus.OK).body(service.findById(id));
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody Cliente cliente){
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cliente));
	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> findById(@RequestBody Cliente cli, @PathVariable Long id){
		
		Optional<Cliente> op = service.findById(id);
		
		if (op.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		Cliente dbCliente = op.get();
		dbCliente.setNombres(cli.getNombres());
		dbCliente.setApellidos(cli.getApellidos());
		dbCliente.setNroDocumento(cli.getNroDocumento());
		dbCliente.setEmail(cli.getEmail());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(dbCliente));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable Long id){
		service.delete(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
}
