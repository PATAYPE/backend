package com.proempresa.app.service;

import com.proempresa.app.entity.Cliente;

public interface ClienteService extends CrudService<Cliente, Long>{

}
