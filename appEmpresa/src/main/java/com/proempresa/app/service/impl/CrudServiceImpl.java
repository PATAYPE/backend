package com.proempresa.app.service.impl;

import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.proempresa.app.service.CrudService;

public class CrudServiceImpl<E, R extends CrudRepository<E, Long>> implements CrudService<E, Long>{

	@Autowired
	protected R repo;
	
	
	@Override
	public E save(E e) {
		return repo.save(e);
	}

	
	@Override
	@Transactional(readOnly = true)
	public Iterable<E> findAll() {
		return repo.findAll();
	}

	
	@Override
	@Transactional(readOnly = true)
	public Optional<E> findById(Long id) {
		return repo.findById(id);
	}

	
	@Override
	public void delete(Long id) {
		repo.deleteById(id);
		
	}

}
