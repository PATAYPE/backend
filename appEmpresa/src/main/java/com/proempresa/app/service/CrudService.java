package com.proempresa.app.service;

import java.util.Optional;

public interface CrudService <E,ID>{

	E save (E t);
	
	Iterable<E> findAll();
	
	Optional<E> findById(Long id);
	
	void delete(Long id);	
}
