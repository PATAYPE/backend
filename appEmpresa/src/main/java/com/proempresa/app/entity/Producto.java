package com.proempresa.app.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Productos")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String descripcion;
	
	private Integer cantidad;
	
	private byte  estado;
	
	@JsonIgnoreProperties(value={"producto"}, allowSetters = true)
	@OneToMany(mappedBy = "producto",fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<DetalleVentas> detalleVentas;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	public byte getEstado() {
		return estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}

	public List<DetalleVentas> getDetalleVentas() {
		return detalleVentas;
	}

	public void setDetalleVentas(List<DetalleVentas> detalleVentas) {
		this.detalleVentas = detalleVentas;
	}

	
	
}
