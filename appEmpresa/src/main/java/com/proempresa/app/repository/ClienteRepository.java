package com.proempresa.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.proempresa.app.entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

}
